class Triangle
	attr_accessor :a, :b, :c
	def initialize(a,b,c)
		@a = a
		@b = b
		@c = c
		@type = ['Ostrougolnij','Tupougolnij','Priamougolnij']
	end

	def triangle_type
		if ((@a**2 + @b**2 == @c**2) or (@b**2 + @c**2 == @a**2) or (@a**2 + @c**2 == @b**2))
			return @type[2]
		elsif ((@a**2 + @b**2 < @c**2) or (@b**2 + @c**2 < @a**2) or (@a**2 + @c**2 < @b**2))
			return @type[1]
		else
			return @type[0]
		end
	end
end
