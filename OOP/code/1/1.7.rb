class LinearEquationEx
	attr_accessor :a, :b
	def initialize(a,b)
		@a = a
		@b = b
	end

	def solve(y)
		@a == 0 ? "Error a should not equals 0" : (y - @b) / a
	end
end