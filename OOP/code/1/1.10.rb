class Binom
	attr_accessor :n, :k
	def initialize(n,k)
		@n = n
		@k = k
	end

	def factorial(num)
		i = 1
		fact = 1
		while i <= num
           fact = fact * i
           i += 1
		end
		fact
	end

	def binom
		factorial(@n) * 1.0 / factorial(@k) * factorial(@n - @k)
	end

	def result
		binom
	end

	private :factorial
	protected :binom
	public :result
end
