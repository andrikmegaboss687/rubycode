class Goods
	attr_accessor :name, :price, :count
	def initialize(name,price,count)
		@name = name
		@price = price
		@count = count
	end

	def summ
		@price * @count
	end

end