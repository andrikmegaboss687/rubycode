class Kupon
	attr_accessor :nominal
	def initialize
		@nominal = [10,20,50,100,500,1000,5000,10000,20000,50000,100000]
	end

	def summ(count,nominal)
		count * nominal
	end
end
