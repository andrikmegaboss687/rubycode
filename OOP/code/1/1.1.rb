class Linear
	attr_accessor :a, :b

	def initialize(a,b)
		@a = a
		@b = b
	end

	def func(x)
		@a * x + @b
	end
end
 