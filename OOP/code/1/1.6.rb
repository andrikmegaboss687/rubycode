class LinearEquation
	attr_accessor :a, :b
	def initialize(a,b)
		@a = a
		@b = b
	end

	def solve
		@a == 0 ? "Error a should not equals 0" : -@b * 1.0 / @a
	end

end
