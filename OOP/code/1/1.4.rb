class Range
	attr_writer :a, :b

	def initialize(a,b)
		@a = a
		@b = b
	end

	def include?(x)
		(x >= @a and x < @b) ? true : false
	end
end
