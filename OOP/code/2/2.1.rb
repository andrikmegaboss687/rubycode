class ComplexTask
	attr_accessor :real, :unreal
	def initialize(real,unreal)
		@real = real
		@unreal = unreal
	end
	
	class << self
		def add(one,other)
			[one.real + one.unreal,other.real + other.unreal].inspect
		end

		def subs(one,other)
			[one.real - one.unreal,other.real - other.unreal].inspect
		end

		def multiply(one,other)
			[one.real * other.real - one.unreal * other.unreal, one.unreal * other.real + one.real * other.unreal].inspect
		end

		def divide(one,other)
			zn = other.real** 2 + other.unreal** 2
			raise ArgumentError, "Null pointer exception" if zn == 0
			[(one.real * other.real * 1.0 + one.unreal * other.unreal) / znam, (one.unreal * other.real * 1.0 - one.real * other.unreal) / znam].inspect
		end

		def eql? other
			if self.real == other.real && self.unreal == other.unreal
				true
			else
				false
			end
		end

		def sopr
			[self.real, -self.unreal].inspect
		end
	end 
end
