# Создавая класс, мы должны уяснить какая часть вашего класса будет доступна окружающему миру.
# Хорошая новость заключается в том, что в Ruby существует группа методов, отвечающих за контроль доступа.
# Существует 3 метода (3 уровня доступа к состоянию объекта) :
# public - Никакого контроля доступа. Все методы по дефолту являютя публичными, кроме initialize.
# protected - доступ ограничен объектами определяемого класса, и подклассами данного класса.
# private - доступ ограничен контекстом текущего объекта.
# Определить уровень доступа того или иного метода можно 2-мя способами:
# Вызвать функции public, protected, private перед методом.
# Вызвать эти же методы с параметрами(именами методов), после их объявления.

class Account
  attr_accessor :balance

  def initialize(balance)
    @balance = balance
  end

end

class Transaction
  def initialize(account_a, account_b)
    @account_a = account_a
    @account_b = account_b
  end

  private
  def debit(account, amount)
    account.balance -= amount
  end

  def credit(account, amount)
      account.balance += amount
  end

  public
  def transfer(amount)
    debit(@account_a, amount)
    credit(@account_b, amount)
  end

end

savings = Account.new(100)
checking = Account.new(200)
trans = Transaction.new(savings, checking)
trans.transfer(50)
