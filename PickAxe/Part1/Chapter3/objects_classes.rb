# Тривиальный пример класса. По сути это только сущность которая будет описывать объект реального мира.
# Имя класса является констанотой, а потому начинается с большой буквы.
# Экземпляры класса создаются методом new.
# Проблема заключается в том, что пока эти экземляры ничем не отличаются друг от друга.
# Более того они не хранят ни какой информации в себе.
# Лучший выход, для решения этой проблемы создать экземпляры при помощи метода initialize.
# Это позволит нам задавать состояние для каждого объекта как только он построен.
# Состсояние экземпляра будет храниться внутри инстансных переменных(@var).
# А поскольку каждый экземпляр будет задаваться своим набором инстансных переменных, то можно говорить об уникальности состояния каждого объекта.
# На первый взгляд может показаться, что параметры, передаваемые внутрь метода и инстансные переменные как то по особенному связаны.
# Однако это не так. Параметры нужны лишь для задания начального значения инстансных переменных.
# Почему мы не использовали puts для вывода объекта в консоль?
# Метод puts выводит лишь строку(в случак объекта метод не знает, что конкретно нужно выводить).
# Для решения этой проблемы нам необходимо перекрыть, вызывающийся по дефолту метод to_s, и передать в него формат строки, необходимый нам.
# Пример показывает силу инстансных переменных. Они хранятся внутри экземпляра и доступны внутри всех его методов.

class BookInStock
  def initialize(isbn, price)
    @isbn = isbn
    @price = Float(price)
  end

  def to_s
    "ISBN: #{ @isbn }, price: #{ @price }"
  end
end

book1 = BookInStock.new('isbn1', 3)
puts book1
book2 = BookInStock.new('isbn2', 3.65)
puts book2
book3 = BookInStock.new('isbn3', 6.57)
puts book3