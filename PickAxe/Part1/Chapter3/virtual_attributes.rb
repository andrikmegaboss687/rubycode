# Достаточно краткое описание виртуальных аттрибутов.
# Иногда, для получения более точной информации об объектах и их состояниях не достаточно аттрибутов.
# Для этого вводится такое понятие как виртуальные аттрибуты.
# По сути это те же самые аттрибуты, которые работают над созданием новых инстансных переменных.
# Значения, возвращаемые этими методами становятся как бы инстансными переменными.
# Отличие состоит лишь в том, что мы не создаем новых инстансных переменных в прямом смысле слова.
# Новый виртуальный аттрибут получается путем изменения существующих инстансных переменных.

class BookInStock

  attr_reader :isbn
  attr_accessor :price

  def initialize(isbn, price)
    @isbn = isbn
    @price = price
  end

  #Виртуальные геттер и сеттер
  def price_in_cents
    Integer(@price * 100 + 0.5)
  end

  def price_in_cents=(cents)
    @price = cents / 100
  end

end

book = BookInStock.new('isbn1', 33.80)
puts "Price = #{ book.price }"
puts "Price in cents = #{ book.price_in_cents }"
book.price_in_cents = 1234
puts "Price = #{ book.price }"
puts "Price in cents = #{ book.price_in_cents }"

