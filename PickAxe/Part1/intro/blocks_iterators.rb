# Самая важная тема это блоки и итераторы.
# Блоки кода - простые вставки кода, заключенные в скобки или словами do, end.
# Причем, если блок многострочный то принято использовать do...end, иначе { }.
# Блок может быть ассоциирован с методом. Метод может вызвать блок, который ассоциированый с ним с помощью ключевого слова yield.
# Метод, передающий параметры, отдает их в блок командой yield(params) и { |params| }
# Основное применение блоков - совместно с итераторами.
# Примеры
def call_block
  puts 'Start of block'
  yield
  yield
  puts 'End of block'
end

def call_block_with_params(who, what)
  yield(who, what)
end

call_block { p 'In the block' }
call_block_with_params('Andy', 'Hello') { |person, phrase| puts "#{ person } says #{ phrase }" }

# Пример итераторов.
animals = %w{cat dog elk pig cow bird}
animals.each { |animal| puts animal }
