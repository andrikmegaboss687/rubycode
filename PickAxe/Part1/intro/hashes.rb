# Создание хеша, обращение к его элементам и их изменение.
# Инициализация хешей происходит 2 способами:
# hash = {} или hash = Hash.new.
# Ключом обычно выступает символ.
# Ключом и значением может выступать объект любого типа.
# По умолчанию, если хешу не было присвоено значение, оно равно числу, переданному в конструктор(при 2 способе создания).

=begin Стиль
Стиль хешей выглядит так:
Многострочный хеш пишется во много строк.
Стиль будет таким: { key: value}
=end

inst_section = {
    cello: 'string',
    clarinet: 'woodwind',
    drum: 'percussion',
    oboe: 'woodwind',
    trumpet: 'brass',
    violin: 'string'
}

p inst_section[:cello]
p inst_section[:oboe]
p inst_section[:bassoon]

hash = Hash.new(0)
p hash[:variable]
p hash[:variable] = hash[:variable] + 1