# Пример создания массива, обращения к его элементам и их изменеие.
# На массиве, как на объекте класса Array могут быть вызваны инстансные методы этого класса.
# Инициализация массива осуществляется 2 способами:
# array = [] или array = Array.new
# Создание массива слов может происходить следующим образом:
# array = %w{}

=begin Стиль
Элементы массива отделяются запятой с пробелом.
Оператор присваивания отделяется пробелами с обеих сторон.
! Имена переменных должны быть информативными (не короткими, не набором символов).
=end
array = [1, 'cat', 3.14]
puts "The first element of array is #{ array[0] }"
array[2] = nil
puts "Now array looks like that: #{ array.inspect }"
words = %w{ ant bee cat dog elk}
puts "Array of words looks like this: #{ words.inspect }"