require_relative "logic"

test1 = Logic.new(years: 5, costs: [20,25,30,35,45,55], revenues: [80,75,65,60,60,55], cost: 40)
test2 = Logic.new(years: 12, costs: [10,10,12,13,14,15,16,17,18,19,20,22,23], revenues: [30,30,29,29,29,28,28,27,27,26,24,23,23], cost: 15)
test3 = Logic.new(years: 10, costs: [1,2,3,4,5,6,7,8,9,10,11], revenues: [11,10,9,8,7,6,5,4,3,2,1], cost: 3)

test2.solve