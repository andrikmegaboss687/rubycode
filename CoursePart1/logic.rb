class Logic
	attr_reader :years,:costs,:revenues,:cost
	def initialize(options)
		@years = options[:years]
		@costs = options[:costs]
		@revenues = options[:revenues]
		@cost = options[:cost]
	end
	
  private
	def bellman(t,k)
	    leave = t == @years ? @revenues[k] - @costs[k] : @revenues[k] - @costs[k] + bellman(t + 1, k + 1).max
	    change = t == @years ? @revenues[0] - @costs[0] - @cost : @revenues[0] - @costs[0] - @cost + bellman(t + 1, 1).max
	    [leave,change]
	end

	def management(t,k)
		u = bellman(t,k)[0] > bellman(t,k)[1] ? true : false
	  u
	end

	public
	def solve
		self.down
		self.up
	end

	def down
		t = @years
	  k = 1
	  puts "First stage"
	  while t > 0
	    puts "YEAR: #{t}" unless t == 1 
		  while k < t
		   	puts "year: #{t}, state: #{k}"
		   	puts "leave = #{bellman(t,k)[0]}"
		   	puts "change = #{bellman(t,k)[1]}"
			  puts [bellman(t,k).max,management(t,k)].inspect + "\n"
			  puts
			  sleep(1)
			  k += 1
		  end
		puts
		t -= 1
		k = 1
	  end
	end
	
	def up
		puts "Optimal solve"
		puts "Maximum benefit is #{bellman(1,0).max}"
		solve = []
		k = 0
		for t in 1..@years do
		   if management(t,k).eql? true
		   solve << management(t,k)
		   k += 1
		   else
           solve << management(t,k)
           k = 1
		   end	
		end
		puts solve.inspect
	end
end