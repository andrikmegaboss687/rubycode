require "rspec"
require_relative "../app/item"
require_relative "../app/virtual_item"
require_relative "../app/item_container"
require_relative "../app/cart"

describe Cart do
	it "add items into a cart" do
		cart = Cart.new("andy")
		item1 = Item.new(name: "pc", price: 200)
		item2 = Item.new(name: "pc", price: 300)
    cart.add_items(item1,item2)
    cart.items.should include(item1,item2)
	end

	it "should removes items from itself"
	it "should count of items in cart"
	it "should creates order with items"
	it "should clear after order"
end
