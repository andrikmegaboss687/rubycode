require "rspec"
require_relative "../app/item"
require_relative "../app/item_container"

class ItemBox
	include ItemContainer
	def initialize
		@items = []
	end
end

describe ItemContainer do
	before(:each) do
	  @box = ItemBox.new
	  @item1 = Item.new(name: "pc", price: 200, weight: 100)
		@item2 = Item.new(name: "avira", price: 250)
	end

	it "should adds item in cart" do 
		@box.add_item(@item1)
		@box.add_item(@item2)
		@box.items.should have(2).items
	end

	it "should remove items from box" do
        @box.add_item(@item1)
        @box.add_item(@item2)
        @box.remove_item.should be(@item2)
	end
end