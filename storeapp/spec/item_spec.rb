require "rspec"
require_relative "../app/item"
require_relative "../app/real_item"

describe Item do
	before(:each) do
		@item = Item.new(price: 200, name: "pc")
	end
	it "price calculate according special formula" do
		@item.price.should == 140.2
	end

	it "should return info about item" do
		@item.to_s.should == "pc:140.2"
	end
end