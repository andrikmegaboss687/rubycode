class Item

	attr_reader :price,:name
	attr_writer :price

	@@discount = 0.1
	def self.discount
		if Time.now.month == 4
			@@discount + 0.2
		else
			@@discount
		end
	end

	def initialize(options = {})
		@price = options[:price]
		@name = options[:name]
	end

	def info
		yield(@price)
		yield(@name)
	end

	def price
		(@price - @price * self.class.discount) + tax if @price
	end

	def to_s
		if self.class == RealItem
			"#{self.name}:#{self.price}:#{self.weight}"
		else
			"#{self.name}:#{self.price}"
		end
	end

	def tax
		tax_type = if self.class == RealItem
			0.2
		else
			0.1
		end
		tax_price = if @price > 100
			0.1
		else
			0.05
		end
		tax_type + tax_price
	end

	private :tax
	
end
