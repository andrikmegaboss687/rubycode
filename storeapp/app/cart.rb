class Cart

	attr_reader :items

	def initialize(owner)
		@items = []
		@owner = owner
	end

	def save_to_file
		File.open("#{@owner}_cart.txt","w") do |f|
			@items.each {|i| f.puts i}
		end
	end

	def add_items(*items)
		@items += items
	end

	def read_from_file
		begin
			File.readlines("#{@owner}_cart.txt").each do |i|
				if i.kind_of? RealItem
					@items << i.to_real_item
				else
					@items << i.to_virtual_item
				end
			end
			@items.uniq!
		rescue Errno::ENOENT
			File.open("#{@owner}_cart.txt","w") {}
			puts "file #{@owner}_cart.txt is created"
		end
	end

	include ItemContainer
	
end