class Order

	attr_reader :items, :placed_at

	def initialize
		@items = []
	end

	def place
		@placed_at = Time.now.utc
	end

	include ItemContainer
end