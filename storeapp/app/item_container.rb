module ItemContainer

	attr_reader :items

	def add_item(item)
		@items << item
	end

	def remove_item
		@items.pop
	end

	def validate
		@items.each do |item|
			puts "Item must have price" if item.price.nil?
			puts "Item must have name" if item.name.nil?
		end
	end

	def delete_invalid_items
		@items.delete_if{|item| item.price.nil? or item.name.nil?}
	end

	def count_valid_items
		@items.count{|item| item.price}
	end
end